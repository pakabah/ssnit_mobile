package com.ssnit.ssnit.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ssnit.ssnit.template.infoTemplate;
import com.ssnit.ssnit.template.notificationTemplate;
import com.ssnit.ssnit.template.recordTemplate;
import com.ssnit.ssnit.template.userTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by pakabah on 10/04/16.
 */
public class DBHelper {

    ssnitDB mssnitDB;

    public DBHelper(Context context)
    {
        mssnitDB = new ssnitDB(context);
    }

    public void deleteAllUsers()
    {
        SQLiteDatabase db = mssnitDB.getWritableDatabase();
        db.delete(ssnitDB.TABLE_USER,null,null);
        db.close();
    }

    public void deleteAllRecords()
    {
        SQLiteDatabase db = mssnitDB.getWritableDatabase();
        db.delete(ssnitDB.TABLE_RECORDS,null,null);
        db.close();
    }

    public void deleteAllInfo()
    {
        SQLiteDatabase db = mssnitDB.getWritableDatabase();
        db.delete(ssnitDB.TABLE_INFO,null,null);
        db.close();
    }

    public void deleteAllNotifications()
    {
        SQLiteDatabase db = mssnitDB.getWritableDatabase();
        db.delete(ssnitDB.TABLE_NOTIFICATIONS,null,null);
        db.close();
    }

    public long insertUser(String id, String firstname, String lastname, String othername, String status, String phone, String issueDate, String sex, String Profile_pic)
    {
        SQLiteDatabase db = mssnitDB.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ssnitDB.COLUMN_ID, id);
        contentValues.put(ssnitDB.COLUMN_FIRSTNAME, firstname);
        contentValues.put(ssnitDB.COLUMN_LASTNAME, lastname);
        contentValues.put(ssnitDB.COLUMN_OTHERNAME, othername);
        contentValues.put(ssnitDB.COLUMN_STATUS, status);
        contentValues.put(ssnitDB.COLUMN_PHONE, phone);
        contentValues.put(ssnitDB.COLUMN_ISSUEDATE,issueDate);
        contentValues.put(ssnitDB.COLUMN_SEX, sex);
        contentValues.put(ssnitDB.COLUMN_PROFILE_PIC, Profile_pic);

        long mid = db.insert(ssnitDB.TABLE_USER,null,contentValues);
        db.close();
        return mid;
    }

    public long insertRecords(String ssnit_id, String company,String amount,String percentage,String date)
    {
        SQLiteDatabase db = mssnitDB.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ssnitDB.COLUMN_SSNIT_ID,ssnit_id);
        contentValues.put(ssnitDB.COLUMN_COMPANY,company);
        contentValues.put(ssnitDB.COLUMN_AMOUNT,amount);
        contentValues.put(ssnitDB.COLUMN_PERCENTAGE,percentage);
        contentValues.put(ssnitDB.COLUMN_DATE,date);

        long mid = db.insert(ssnitDB.TABLE_RECORDS,null,contentValues);
        db.close();
        return mid;
    }

    public long insertInfo(String info, String date)
    {
        SQLiteDatabase db = mssnitDB.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ssnitDB.COLUMN_INFO,info);
        contentValues.put(ssnitDB.COLUMN_INFO_DATE,date);

        long mid = db.insert(ssnitDB.TABLE_INFO,null,contentValues);
        db.close();
        return mid;
    }

    public long insertNotification(String message)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());
        SQLiteDatabase db = mssnitDB.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(ssnitDB.COULMN_MESSAGE,message);
        contentValues.put(ssnitDB.COLUMN_NOTE_DATE,currentDateandTime);

        Log.e("Inserting Message", message);

        long mid = db.insert(ssnitDB.TABLE_NOTIFICATIONS,null,contentValues);
        db.close();
        return mid;
    }


    public userTemplate getCardInfo()
    {
        userTemplate data = new userTemplate();
        SQLiteDatabase db = mssnitDB.getWritableDatabase();
        String[] columns = {ssnitDB.COLUMN_ID, ssnitDB.COLUMN_FIRSTNAME,ssnitDB.COLUMN_LASTNAME,ssnitDB.COLUMN_OTHERNAME, ssnitDB.COLUMN_STATUS, ssnitDB.COLUMN_PHONE, ssnitDB.COLUMN_ISSUEDATE, ssnitDB.COLUMN_SEX,ssnitDB.COLUMN_PROFILE_PIC};
        Cursor cursor = db.query(ssnitDB.TABLE_USER,columns,null,null,null,null,null);
        while(cursor.moveToNext())
        {
            int index  = cursor.getColumnIndex(ssnitDB.COLUMN_ID);
            int index1  = cursor.getColumnIndex(ssnitDB.COLUMN_FIRSTNAME);
            int index2  = cursor.getColumnIndex(ssnitDB.COLUMN_LASTNAME);
            int index3 = cursor.getColumnIndex(ssnitDB.COLUMN_OTHERNAME);
            int index4  = cursor.getColumnIndex(ssnitDB.COLUMN_STATUS);
            int index5 = cursor.getColumnIndex(ssnitDB.COLUMN_PHONE);
            int index6 = cursor.getColumnIndex(ssnitDB.COLUMN_SEX);
            int index7 = cursor.getColumnIndex(ssnitDB.COLUMN_ISSUEDATE);
            int index8 = cursor.getColumnIndex(ssnitDB.COLUMN_PROFILE_PIC);


            data.firstname = cursor.getString(index1);
            data.id = cursor.getString(index);
            data.lastname = cursor.getString(index2);
            data.othername = cursor.getString(index3);
            data.status = cursor.getString(index4);
            data.phone = cursor.getString(index5);
            data.sex = cursor.getString(index6);
            data.issueDate = cursor.getString(index7);
            data.profile_pic = cursor.getString(index8);

        }
        db.close();

        return data;
    }

    public ArrayList<recordTemplate> getRecords()
    {
        ArrayList<recordTemplate> recordTemplate = new ArrayList<>();
        SQLiteDatabase db = mssnitDB.getWritableDatabase();
        String[] columns = {ssnitDB.COLUMN_SSNIT_ID,ssnitDB.COLUMN_REC_ID,ssnitDB.COLUMN_COMPANY,ssnitDB.COLUMN_AMOUNT,ssnitDB.COLUMN_PERCENTAGE,ssnitDB.COLUMN_DATE};
        Cursor cursor = db.query(ssnitDB.TABLE_RECORDS,columns,null,null,null,null,null);

        while(cursor.moveToNext()) {
            int index = cursor.getColumnIndex(ssnitDB.COLUMN_REC_ID);
            int index1 = cursor.getColumnIndex(ssnitDB.COLUMN_SSNIT_ID);
            int index2 = cursor.getColumnIndex(ssnitDB.COLUMN_COMPANY);
            int index3 = cursor.getColumnIndex(ssnitDB.COLUMN_AMOUNT);
            int index4 = cursor.getColumnIndex(ssnitDB.COLUMN_PERCENTAGE);
            int index5 = cursor.getColumnIndex(ssnitDB.COLUMN_DATE);

            recordTemplate recordTemplate1 = new recordTemplate();
            recordTemplate1.Amount = cursor.getString(index3);
            recordTemplate1.Date = cursor.getString(index5);
            recordTemplate1.Percentage = cursor.getString(index4);
            recordTemplate1.Employer = cursor.getString(index2);
            Log.e("Records Query", "Amount "+ recordTemplate1.Amount);

            recordTemplate.add(recordTemplate1);
        }
        db.close();

        return recordTemplate;

    }

    public ArrayList<infoTemplate> getInfo()
    {
        ArrayList<infoTemplate> infoTemplates = new ArrayList<>();
        SQLiteDatabase db = mssnitDB.getWritableDatabase();
        String[] columns = {ssnitDB.COLUMN_INFO_ID,ssnitDB.COLUMN_INFO_DATE,ssnitDB.COLUMN_INFO};
        Cursor cursor = db.query(ssnitDB.TABLE_INFO,columns,null,null,null,null,null);
        while(cursor.moveToNext()) {
            int index = cursor.getColumnIndex(ssnitDB.COLUMN_INFO_ID);
            int index1 = cursor.getColumnIndex(ssnitDB.COLUMN_INFO_DATE);
            int index2 = cursor.getColumnIndex(ssnitDB.COLUMN_INFO);

            infoTemplate infoTemplate = new infoTemplate();
            infoTemplate.date = cursor.getString(index1);
            infoTemplate.information = cursor.getString(index2);

            Log.e("Info Query", "Info "+ infoTemplate.information);
            infoTemplates.add(infoTemplate);
        }

        db.close();
        return infoTemplates;
    }

    public ArrayList<notificationTemplate> getMessages()
    {
        ArrayList<notificationTemplate> notificationTemplates = new ArrayList<>();
        SQLiteDatabase db = mssnitDB.getWritableDatabase();
        String[] columns = {ssnitDB.COULMN_MESSAGE,ssnitDB.COLUMN_NOTE_DATE};
        Cursor cursor = db.query(ssnitDB.TABLE_NOTIFICATIONS,columns,null,null,null,null,null);
        while(cursor.moveToNext()) {
            int index = cursor.getColumnIndex(ssnitDB.COULMN_MESSAGE);
            int index1 = cursor.getColumnIndex(ssnitDB.COLUMN_NOTE_DATE);

            notificationTemplate notificationTemplate = new notificationTemplate();
            notificationTemplate.information = cursor.getString(index);
            Log.e("Notification Message", cursor.getString(index));
            notificationTemplate.date = cursor.getString(index1);

            notificationTemplates.add(notificationTemplate);
        }

        db.close();
        return notificationTemplates;
    }

    class ssnitDB extends SQLiteOpenHelper
    {

        private static final int DATABASE_VERSION = 2;
        private static final String DATABASE_NAME = "ssnit.db";

        public static final String TABLE_USER = "user";
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_FIRSTNAME = "firstname";
        public static final String COLUMN_LASTNAME = "lastname";
        public static final String COLUMN_OTHERNAME = "othername";
        public static final String COLUMN_PHONE = "phone";
        public static final String COLUMN_STATUS = "status";
        public static final String COLUMN_ISSUEDATE = "issuedate";
        public static final String COLUMN_PROFILE_PIC = "profile_pic";
        public static final String COLUMN_SEX = "sex";


        public static final String DATABASE_USERS = "create table " +
                TABLE_USER + "(" + COLUMN_ID + " text PRIMARY KEY, "
                + COLUMN_FIRSTNAME + " text, "
                + COLUMN_LASTNAME + " text, "
                + COLUMN_OTHERNAME + " text, "
                + COLUMN_ISSUEDATE + " text, "
                + COLUMN_SEX + " text, "
                + COLUMN_PHONE + " text, "
                + COLUMN_PROFILE_PIC + " text, "
                + COLUMN_STATUS +" text);";


        public static final String TABLE_INFO = "info_table";
        public static final String COLUMN_INFO_ID = "_id";
        public static final String COLUMN_INFO_DATE = "date";
        public static final String COLUMN_INFO = "info";

        public static final String DATABASE_INFO = "create table " +
                TABLE_INFO + "(" + COLUMN_INFO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_INFO_DATE + " text, "
                + COLUMN_INFO + " text);";

        public static final String TABLE_RECORDS = "records";
        public static final String COLUMN_SSNIT_ID = "ssnit_id";
        public static final String COLUMN_REC_ID = "_id";
        public static final String COLUMN_COMPANY = "company";
        public static final String COLUMN_AMOUNT  = "amount";
        public static final String COLUMN_PERCENTAGE = "percentage";
        public static final String COLUMN_DATE = "date";


        public static final String DATABASE_RECORDS = "create table " +
                TABLE_RECORDS + "(" + COLUMN_REC_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_SSNIT_ID + " text, "
                + COLUMN_COMPANY + " text, "
                + COLUMN_AMOUNT + " text, "
                + COLUMN_PERCENTAGE + " text, "
                + COLUMN_DATE +" text);";

        public static final String TABLE_NOTIFICATIONS = "notification";
        public static final String COLUMN_NOTIFICATION_ID = "_id";
        public static final String COULMN_MESSAGE = "message";
        public static final String COLUMN_NOTE_DATE = "date";

        public static final String DATABASE_NOTIFICATION = "create table "+
                TABLE_NOTIFICATIONS + "(" + COLUMN_NOTIFICATION_ID +  " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COULMN_MESSAGE + " text, "
                + COLUMN_NOTE_DATE + " text);";


        public ssnitDB(Context context) {
            super(context,DATABASE_NAME,null,DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_USERS);
            db.execSQL(DATABASE_RECORDS);
            db.execSQL(DATABASE_INFO);
            db.execSQL(DATABASE_NOTIFICATION);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECORDS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_INFO);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTIFICATIONS);

        }
    }
}
