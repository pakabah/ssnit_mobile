package com.ssnit.ssnit;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class FindMap extends AppCompatActivity implements OnMapReadyCallback {

    private  GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_map);
        initToolbar();
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Find Office");
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(5.9343531,-0.6651134))
                .title("SSNIT Office"));
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(7.3350819,-2.325846))
                .title("SSNIT Office"));
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(6.6942189,-1.6578836))
                .title("SSNIT Office"));
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(6.6942189,-1.6578836))
                .title("SSNIT Office"));
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(5.5577191,-0.2103662))
                .title("SSNIT Office"));

        LatLng latLng = new LatLng( 5.5599142,-0.202464);
//
//        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
//        mMap.addMarker(new MarkerOptions().position(latLng).title("Head Office"));
//        mMap.animateCamera(cameraUpdate);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

        // Zoom in, animating the camera.
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }
}
