package com.ssnit.ssnit.calc;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.ssnit.ssnit.R;

import java.text.DecimalFormat;

public class WDS extends AppCompatActivity {

    Spinner MCM;
    EditText total;
    EditText highest,second,third;
    Button Calculate;
    TextView sBenefit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wds);
        initToolbar();

        MCM = (Spinner) findViewById(R.id.min);
        total = (EditText) findViewById(R.id.total);
        highest = (EditText) findViewById(R.id.highOne);
        second = (EditText) findViewById(R.id.secondHighest);
        third = (EditText) findViewById(R.id.thirdHighest);
        Calculate = (Button) findViewById(R.id.calc);
        sBenefit = (TextView) findViewById(R.id.sBenefit);

        Calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int totals = Integer.parseInt(total.getText().toString());
                double first = Double.parseDouble(highest.getText().toString());
                double sec = Double.parseDouble(second.getText().toString());
                double th = Double.parseDouble(third.getText().toString());
                String under = MCM.getSelectedItem().toString();
                if(under.equals("Min. of 240 months under PNDC 247"))
                {
                    under = "PNDC";
                }
                else
                {
                    under = "766";
                }

                String finalValue = "GH₵ " + CalculateAll(under,totals,first,sec,th);
                sBenefit.setText(finalValue);
            }
        });

    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Worker Death Survivor's Benefit");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }


    public double pensionRight(String Under, int total)
    {
        double finalValue = 0 ;
        if(Under.equals("PNDC"))
        {
            finalValue = 0.5 + (total - 240 ) * 0.00125;
        }
        else
        {
         finalValue = 0.375 + (total -180) * 0.0009375;
        }
        return finalValue;
    }

    public double averageOf3(Double Salary1, Double Salary2, Double Salary3)
    {
        return (Salary1 + Salary2 + Salary3) / 3;
    }

    public double annualPension(Double PensionRight, Double averageOf3)
    {
        return PensionRight * averageOf3;
    }

    public double monthlyPension(Double annualPension)
    {
        return annualPension/12;
    }


    public double survivorsBenefit(String Under,Double MonthlyPension)
    {
        double finalValue = 0;

        if(Under.equals("PNDC"))
        {
            finalValue = 83.67653 * MonthlyPension;
        }
        else
        {
            finalValue = 93.05744 * MonthlyPension;
        }
        return finalValue;
    }

    public String CalculateAll(String Under, int total, Double Salary1, Double Salary2, Double Salary3)
    {
        double monthly =   monthlyPension(annualPension(pensionRight(Under,total),averageOf3(Salary1,Salary2,Salary3)));
        DecimalFormat df = new DecimalFormat("#.00");
        return df.format(survivorsBenefit(Under,monthly));
    }
}
