package com.ssnit.ssnit.calc;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.ssnit.ssnit.R;

public class PDSB extends AppCompatActivity {

    EditText age, mpd;
    Spinner Under;
    Button calculate;
    TextView pdsbBenefit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdsb);

        initToolbar();

        age = (EditText) findViewById(R.id.editText);
        Under = (Spinner) findViewById(R.id.spinner);
        mpd = (EditText) findViewById(R.id.editText2);
        calculate = (Button) findViewById(R.id.calcPDSB);
        pdsbBenefit = (TextView) findViewById(R.id.pdsbBenefit);

        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int mAge = Integer.parseInt(age.getText().toString());
                String mUnder = Under.getSelectedItem().toString();
                double mMpd = Double.parseDouble(mpd.getText().toString());
                if(mUnder.equals("PNDC 247"))
                {
                    mUnder = "PNDC";
                }
                else
                {
                    mUnder = "766";
                }
                String finalVal =  "GH₵ "+ CalculateAll(mUnder,mAge,mMpd);
                pdsbBenefit.setText(finalVal);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Pensioner Death Survivor's Benefit");
        }
    }

    public int UPP(String mUnder)
    {
        if(mUnder.equals("PNDC"))
        {
            return 144;
        }
        else
        {
            return 180;
        }
    }

    public int NMUPP(String mUnder,int age)
    {
        if(mUnder.equals("PNDC"))
        {
            Log.e("NMUPP PNDC", " "+ ((72- age)* 12) + 9);
           return ((72- age)*12) + 9;

        }
        else
        {
            Log.e("NMUPP ACT ", " "+ ((75- age)*12)+9);
            return (((75-age)*12) + 9);
        }
    }

    public double annuityFactor(int NMUPP)
    {
        double finalValue = 0;

       finalValue =  (1- Math.pow((1 + (0.1/12) ), - NMUPP)) / (0.1/12);
        Log.e("Annuity Factor", ""+ finalValue);

        return finalValue;
    }

    public int survivorsBenefit(double annuityFactor, double MP)
    {
        return (int) (annuityFactor * MP);
    }

    public String CalculateAll(String Under, int age,double MP)
    {

        return Integer.toString(survivorsBenefit(annuityFactor(NMUPP(Under,age)), MP));
    }
}
