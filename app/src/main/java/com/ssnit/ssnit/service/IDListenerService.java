package com.ssnit.ssnit.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.ssnit.ssnit.R;
import com.ssnit.ssnit.api.apiCall;

import java.io.IOException;

import static com.ssnit.ssnit.MainActivity.DEFAULT;

public class IDListenerService extends IntentService {

    private static final String TAG = "RegIntentService";
    String projectId = "ssnit-1260";

    public IDListenerService() {
         super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        InstanceID instanceID = InstanceID.getInstance(this);
        String token = null;
        Log.e("First", "IDL Listener");
        try {
        token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, true).apply();
            apiCall apiCall = new apiCall();
            apiCall.sendRegistration(token, getSSNITID());

            Log.e("GCD TOKEN", token);
        } catch (IOException e) {
            Log.d(TAG, "Failed to complete token refresh", e);
            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false).apply();
            e.printStackTrace();
        }

    }

    private String getSSNITID()
    {
        SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
        return sharedPreferences.getString("ssnit_id", DEFAULT);
    }
}
