package com.ssnit.ssnit.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ssnit.ssnit.R;
import com.ssnit.ssnit.template.notificationTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by pakabah on 28/03/16.
 */
public class notificationAdapter extends RecyclerView.Adapter<notificationAdapter.ViewHolder> {

    private List<notificationTemplate> notificationTemplates;
    private Context context;

    public notificationAdapter(List<notificationTemplate> notificationTemplate, Context context)
    {
        this.notificationTemplates = new ArrayList<>();
        this.notificationTemplates.addAll(notificationTemplate);
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_notification,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        notificationTemplate notificationTemplate = notificationTemplates.get(position);

        SimpleDateFormat sim =new SimpleDateFormat("EEE, dd MMM yyyy");
        try {
            Date d = new SimpleDateFormat("dd-MM-yyyy").parse(notificationTemplate.date);
            String newDate = sim.format(d);
            holder.date.setText(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.information.setText(notificationTemplate.information);

    }

    @Override
    public int getItemCount() {
        return notificationTemplates.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView date,information;
        CardView infoCard;
        public ViewHolder(View itemView) {
            super(itemView);
            date = (TextView) itemView.findViewById(R.id.date);
            information = (TextView) itemView.findViewById(R.id.notificationMsg);
            infoCard = (CardView) itemView.findViewById(R.id.cardInfo);
        }
    }
}
