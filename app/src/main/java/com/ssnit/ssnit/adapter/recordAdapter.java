package com.ssnit.ssnit.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ssnit.ssnit.R;
import com.ssnit.ssnit.RecordDetails;
import com.ssnit.ssnit.template.recordTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pakabah on 23/03/16.
 */
public class recordAdapter extends RecyclerView.Adapter<recordAdapter.ViewHolder>{

    List<recordTemplate> recordTemplates;
Context context;

    public recordAdapter(List<recordTemplate> recordTemplates, Context context)
    {
        this.recordTemplates = new ArrayList<>();
        this.recordTemplates.addAll(recordTemplates);
        this.context = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_record,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        recordTemplate recordTemplate = recordTemplates.get(position);
        holder.Employer.setText(recordTemplate.Employer);
        String perc = recordTemplate.Percentage + "%";
        holder.Percentage.setText(perc);
        String amt = recordTemplate.Amount + "GHC";

        holder.Amount.setText(amt);
        holder.Date.setText(recordTemplate.Date);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RecordDetails.class);
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return recordTemplates.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView Amount;
        TextView Percentage;
        TextView Date;
        TextView Employer;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);
            Amount = (TextView) itemView.findViewById(R.id.amount);
            Percentage = (TextView) itemView.findViewById(R.id.percentage);
            Date = (TextView) itemView.findViewById(R.id.date);
            Employer = (TextView) itemView.findViewById(R.id.employer);
            cardView = (CardView) itemView.findViewById(R.id.recordsCard);
        }
    }
}
