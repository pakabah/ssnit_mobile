package com.ssnit.ssnit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ssnit.ssnit.adapter.notificationAdapter;
import com.ssnit.ssnit.api.apiCall;
import com.ssnit.ssnit.db.DBHelper;
import com.ssnit.ssnit.template.notificationTemplate;

import java.util.List;

public class Notification extends AppCompatActivity {

    private RecyclerView recyclerView;

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if(bundle!=null) {

                notificationAdapter notificationAdapter = new notificationAdapter(initializeData(),getApplicationContext());
                recyclerView.setAdapter(notificationAdapter);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        apiCall apiCall = new apiCall(getApplicationContext());
        apiCall.getNotification();

        registerReceiver(broadcastReceiver, new IntentFilter(apiCall.NOTIF));

         recyclerView = (RecyclerView) findViewById(R.id.relNotification);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        assert recyclerView != null;

        recyclerView.setLayoutManager(layoutManager);
        notificationAdapter notificationAdapter = new notificationAdapter(initializeData(),getApplicationContext());
        recyclerView.setAdapter(notificationAdapter);
    }

    private List<notificationTemplate> initializeData()
    {
        DBHelper dbHelper = new DBHelper(getApplicationContext());
        return dbHelper.getMessages();
    }

    @Override
    public void onDestroy() {
        if(broadcastReceiver !=null)
        {
            unregisterReceiver(broadcastReceiver);
        }
        super.onDestroy();
    }
}
