package com.ssnit.ssnit.template;

/**
 * Created by pakabah on 23/03/16.
 */
public class recordTemplate {

    public String Date;
    public String Amount;
    public String Percentage;
    public String Employer;

    public recordTemplate(String Date, String Amount, String Percentage, String Employer)
    {
        this.Date = Date;
        this.Amount = Amount;
        this.Percentage = Percentage;
        this.Employer = Employer;
    }

    public recordTemplate()
    {

    }
}
