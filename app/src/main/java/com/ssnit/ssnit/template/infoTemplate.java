package com.ssnit.ssnit.template;

/**
 * Created by pakabah on 28/03/16.
 */
public class infoTemplate {

    public String date;
    public String dayish;
    public String month;
    public String information;

    public infoTemplate(String date, String dayish, String month, String information)
    {
        this.date = date;
        this.dayish = dayish;
        this.month = month;
        this.information = information;
    }

    public infoTemplate()
    {

    }
}
